package su.sviridoff.net.enums;

public enum NetworkType {

    Private("Private network"), Public("Public network");

    private final String value;

    NetworkType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
