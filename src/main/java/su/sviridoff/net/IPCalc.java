package su.sviridoff.net;

import ru.sviridoff.framework.logger.Logger;
import su.sviridoff.net.enums.NetworkClass;
import su.sviridoff.net.enums.NetworkType;

import java.util.Arrays;

public class IPCalc {

    private final static Logger logger = new Logger(IPCalc.class.getName());

    private String fstOct, scdOct, thdOct, fthOct, netBits, ipAddress;
    private int fstOctI, scdOctI, thdOctI, fthOctI, netBitsI;

    public IPCalc(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getIpAddress() {
        return this.ipAddress;
    }

    public String getBinIpAddress() {
        return toBinary(this.ipAddress);
    }

    private void toOctets(String address) {
        int dots = 0, dotPos = 0, slash = 0;
        for (int i = 0; i < address.length(); i++) {
            if (address.charAt(i) == 0x2F) // 0x2F - /
                slash ++;
        }
        for (int i = 0; i < address.length(); i++) {
            if ((address.charAt(i) == '.') || i == (address.length() - 1)) {
                boolean isOctNotEmpty = !address.substring(dotPos, i).isEmpty();
                if (dots == 0 && isOctNotEmpty)
                    fstOct = address.substring(dotPos, i);
                if (dots == 1 && isOctNotEmpty)
                    scdOct = address.substring(dotPos, i);
                if (dots == 2 && isOctNotEmpty)
                    thdOct = address.substring(dotPos, i);
                if (dots == 3 && !address.substring(dotPos, i + 1).isEmpty() && slash == 0)
                    fthOct = address.substring(dotPos, i + 1);
                dots ++;
                dotPos = i + 1;
            }
            if (address.charAt(i) == '/' && dots == 3 && !address.substring(dotPos, i).isEmpty()) {
                if (!address.substring(dotPos, i).isEmpty() && slash == 1) {
                    fthOct = address.substring(dotPos, i);
                }
                netBits = address.substring(i + 1);
            }
        }
        if (slash == 0) {
            netBits = "";
        }
    }

    private String toBinary(String toConvert) {
        toOctets(toConvert);
        fstOctI = Integer.parseInt(fstOct);
        scdOctI = Integer.parseInt(scdOct);
        thdOctI = Integer.parseInt(thdOct);
        fthOctI = Integer.parseInt(fthOct);
        String binIp;
        if (netBits.isEmpty()) {
            binIp = Integer.toBinaryString(fstOctI) + "." + Integer.toBinaryString(scdOctI) + "." + Integer.toBinaryString(thdOctI) + "." + Integer.toBinaryString(fthOctI);
        } else {
            netBitsI = Integer.parseInt(netBits);
            binIp = Integer.toBinaryString(fstOctI) + "." + Integer.toBinaryString(scdOctI) + "." + Integer.toBinaryString(thdOctI) + "." + Integer.toBinaryString(fthOctI) + "/" + Integer.toBinaryString(netBitsI);
        }
        return binIp;
    }

    public boolean verify() {
        int dots = 0, slash = 0;
        for(int i = 0; i < ipAddress.length(); i++) {
            if(ipAddress.charAt(i) < 0x2E || ipAddress.charAt(i) > 0x39) {
                return false;
            }
            if(ipAddress.charAt(i) == 0x2E)
                dots++;
            if(ipAddress.charAt(i) == 0x2F)
                slash++;
        }
        if(dots != 3)
            return false;
        if(slash > 1 || slash < 0)
            return false;
        dots = 0;
        int dotPos = 0;
        for(int i = 0; i < ipAddress.length(); i++) {
            if(ipAddress.charAt(i) == '.' || i == ipAddress.length() - 1) {
                if(dots == 0 && !ipAddress.substring(dotPos, i).isEmpty())
                    fstOct = ipAddress.substring(dotPos, i);
                if(dots == 1 && !ipAddress.substring(dotPos, i).isEmpty())
                    scdOct = ipAddress.substring(dotPos, i);
                if(dots == 2 && !ipAddress.substring(dotPos, i).isEmpty())
                    thdOct = ipAddress.substring(dotPos, i);
                if(dots == 3 && !ipAddress.substring(dotPos, i + 1).isEmpty() && slash == 0)
                    fthOct = ipAddress.substring(dotPos, i+1);
                dots++;
                dotPos = i+1;
            }
            if(ipAddress.charAt(i) == '/' && dots == 3 && !ipAddress.substring(dotPos, i).isEmpty()) {
                if(!ipAddress.substring(dotPos, i).isEmpty() && slash == 1)
                    fthOct  = ipAddress.substring(dotPos, i);
                netBits = ipAddress.substring(i+1, ipAddress.length());
            }
        }

        fstOctI = Integer.parseInt(fstOct);
        scdOctI = Integer.parseInt(scdOct);
        thdOctI = Integer.parseInt(thdOct);
        fthOctI = Integer.parseInt(fthOct);
        if(!netBits.isEmpty())
            netBitsI = Integer.parseInt(netBits);

        if(fstOctI > 239 || fstOctI < 0)
            return false;
        else if(scdOctI > 255 || scdOctI < 0)
            return false;
        else if(thdOctI > 255 || thdOctI < 0)
            return false;
        else if(fthOctI > 255 || fthOctI < 0)
            return false;
        else return (netBitsI <= 30 && netBitsI >= 7) || netBits.isEmpty();
    }

    public String getIpAddressClass() {
        toOctets(ipAddress);
        int intFirstOctet;
        intFirstOctet = Integer.parseInt(fstOct);
        String ipClass;

        if(intFirstOctet < 128)
            ipClass = NetworkClass.A.name();
        else if(intFirstOctet < 192)
            ipClass = NetworkClass.B.name();
        else if(intFirstOctet < 224)
            ipClass = NetworkClass.C.name();
        else ipClass = NetworkClass.D.name();

        return ipClass;
    }

    public String getBinNetMask() {
        toOctets(ipAddress);
        String netMask;
        char[] cAA = new char[35];
        if(netBits.isEmpty()) {
            int intFirstOctet;
            intFirstOctet = Integer.parseInt(fstOct);

            if(intFirstOctet < 128)
                netMask = "11111111.00000000.00000000.00000000";
            else if(intFirstOctet < 192)
                netMask = "11111111.11111111.00000000.00000000";
            else if(intFirstOctet < 224)
                netMask = "11111111.11111111.11111111.00000000";
            else netMask = "11111111.11111111.11111111.11111111";

        }
        else {
            int dotC = 0;
            int wOBit = 0;
            for(int i = 0; i < 35; i++) {
                if(wOBit < netBitsI && dotC < 8) {
                    cAA[i] = '1';
                    dotC++;
                    wOBit++;
                }
                else if(wOBit >= netBitsI && dotC < 8) {
                    cAA[i] = '0';
                    dotC++;
                    wOBit++;
                }
                else if(dotC == 8) {
                    cAA[i] = '.';
                    dotC = 0;
                }
            }
            netMask = new String(cAA);
        }

        return netMask;
    }

    public String getDecNetMask() {
        String decNetMask, binNetMask;
        binNetMask = getBinNetMask();
        return getString(binNetMask);
    }

    public String getDecNetwork() {
        String decNetworkA = "";
        String netMask = getDecNetMask();

        int fAOctet, sAOctet, tAOctet, lAOctet;
        int fMOctet, sMOctet, tMOctet, lMOctet;
        int fNOctet, sNOctet, tNOctet, lNOctet;

        toOctets(ipAddress);
        fAOctet = Integer.parseInt(fstOct);
        sAOctet = Integer.parseInt(scdOct);
        tAOctet = Integer.parseInt(thdOct);
        lAOctet = Integer.parseInt(fthOct);

        toOctets(netMask);
        fMOctet = Integer.parseInt(fstOct);
        sMOctet = Integer.parseInt(scdOct);
        tMOctet = Integer.parseInt(thdOct);
        lMOctet = Integer.parseInt(fthOct);

        fNOctet = fAOctet & fMOctet;
        sNOctet = sAOctet & sMOctet;
        tNOctet = tAOctet & tMOctet;
        lNOctet = lAOctet & lMOctet;
        decNetworkA = fNOctet + "." + sNOctet + "." + tNOctet + "." + lNOctet;
        return decNetworkA;
    }

    public String getBinNetwork() {
        return toBinary(getDecNetwork());
    }

    public String getBinWildCard() {
        toOctets(ipAddress);
        String wildCard, netMask;
        netMask = getBinNetMask();
        char[] cAA;
        cAA = netMask.toCharArray();
        for(int i=0;i<35;i++) {
            if(cAA[i]=='1') {
                cAA[i] = '0';
            }
            else if(cAA[i]=='0') {
                cAA[i] = '1';
            }
        }
        wildCard = new String(cAA);
        return wildCard;
    }

    public String getDecWildCard() {
        String decWildCard, binWildCard;
        binWildCard = getBinWildCard();
        return getString(binWildCard);
    }

    private String getString(String binWildCard) {
        String decWildCard;
        toOctets(binWildCard);
        fstOctI = Integer.parseInt(fstOct, 2);
        scdOctI = Integer.parseInt(scdOct, 2);
        thdOctI  = Integer.parseInt(thdOct, 2);
        fthOctI = Integer.parseInt(fthOct, 2);
        decWildCard = fstOctI + "." + scdOctI + "." + thdOctI + "." + fthOctI;
        toOctets(ipAddress);
        return decWildCard;
    }

    public String getDecBroadcast() {
        String decBroadcastA;
        String netMask = getDecWildCard();
        int fAOctet, sAOctet, tAOctet, lAOctet;
        int fWOctet, sWOctet, tWOctet, lWOctet;
        int fNOctet, sNOctet, tNOctet, lNOctet;
        toOctets(ipAddress);
        fAOctet = Integer.parseInt(fstOct);
        sAOctet = Integer.parseInt(scdOct);
        tAOctet = Integer.parseInt(thdOct);
        lAOctet = Integer.parseInt(fthOct);
        toOctets(netMask);
        fWOctet = Integer.parseInt(fstOct);
        sWOctet = Integer.parseInt(scdOct);
        tWOctet = Integer.parseInt(thdOct);
        lWOctet = Integer.parseInt(fthOct);
        fNOctet = fAOctet | fWOctet;
        sNOctet = sAOctet | sWOctet;
        tNOctet = tAOctet | tWOctet;
        lNOctet = lAOctet | lWOctet;
        toOctets(ipAddress);
        decBroadcastA = fNOctet + "." + sNOctet + "." + tNOctet + "." + lNOctet;
        return decBroadcastA;
    }

    public String getBinBroadcast() {
        return toBinary(getDecBroadcast());
    }

    public String getNHost() {
        toOctets(ipAddress);
        double nHost=1;
        int numHost;
        if(netBits.isEmpty()) {
            if(getIpAddressClass().equals(NetworkClass.A.name()))
                nHost = 16777214;
            if(getIpAddressClass().equals(NetworkClass.B.name()))
                nHost = 65534;
            if(getIpAddressClass().equals(NetworkClass.C.name()) || getIpAddressClass().equals(NetworkClass.D.name()))
                nHost = 254;
        }
        else {
            netBitsI = Integer.parseInt(netBits);
            nHost = (int) (Math.pow(2, 32-netBitsI)-2);
        }
        numHost = (int) nHost;
        return numHost+"";
    }

    public String getNNet() {
        toOctets(ipAddress);
        double nNet=1;
        int numNet;
        if(netBits.isEmpty()) {
            if(getIpAddressClass().equals(NetworkClass.A.name()))
                nNet = 128;
            if(getIpAddressClass().equals(NetworkClass.B.name()))
                nNet = 16384;
            if(getIpAddressClass().equals(NetworkClass.C.name()) || getIpAddressClass().equals(NetworkClass.D.name()))
                nNet = 2097152;
        }
        else {
            netBitsI = Integer.parseInt(netBits);
            nNet = Math.pow(2, netBitsI);
        }
        numNet = (int) nNet;
        return numNet+"";
    }

    public String getDecMinHost() {
        String network = getDecNetwork();
        toOctets(network);
        fthOctI = Integer.parseInt(fthOct);
        fthOctI++;
        fthOct = Integer.toString(fthOctI);
        String minHost = fstOct + "." + scdOct + "." + thdOct + "." + fthOct;
        toOctets(ipAddress);
        return minHost;
    }

    public String getBinMinHost() {
        String binMinHost = getDecMinHost();
        return toBinary(binMinHost);
    }

    public String getDecMaxHost() {
        String network = getDecBroadcast();
        toOctets(network);
        fthOctI = Integer.parseInt(fthOct);
        fthOctI--;
        fthOct = Integer.toString(fthOctI);
        String maxHost = fstOct + "." + scdOct + "." + thdOct + "." + fthOct;
        toOctets(ipAddress);
        return maxHost;
    }

    public String getBinMaxHost() {
        String binMaxHost = getDecMaxHost();
        return toBinary(binMaxHost);
    }

    public String getTypeInternet() {
        String internet = "";
        toOctets(ipAddress);
        fstOctI = Integer.parseInt(fstOct);
        scdOctI = Integer.parseInt(scdOct);
        if(fstOctI == 10)
            internet = NetworkType.Private.getValue();
        else if(fstOctI == 172 && (scdOctI > 15 && scdOctI < 32))
            internet = NetworkType.Private.getValue();
        else if(fstOctI == 192 && scdOctI == 168)
            internet = NetworkType.Private.getValue();
        else
            internet = NetworkType.Public.getValue();

        return internet;
    }

    public static String visualizeAddress(String address) {
        String tmpAddress = address.split("/")[0];
        String[] octets = tmpAddress.split("\\.");
        StringBuilder retAddress = new StringBuilder();
        Arrays.asList(octets).forEach(octet -> {
            if (octet.length() < 8) {
                StringBuilder tmpOctet = new StringBuilder();
                for (int i = 0; i < 8 - octet.length(); i++) {
                    tmpOctet.append("0");
                }
                tmpOctet.append(octet);
                octet = tmpOctet.toString();
                logger.warning(octet);
            }
            retAddress.append(octet).append(".");
        });
        return retAddress.toString();
    }

}
