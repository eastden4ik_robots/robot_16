package su.sviridoff.net.telebot;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.sviridoff.framework.logger.Logger;
import su.sviridoff.net.IPCalc;

public class IPCalcBot extends TelegramLongPollingBot {

    private String TOKEN;
    private String USERNAME;

    private Logger log = new Logger(IPCalcBot.class.getName());

    public IPCalcBot(String TOKEN, String USERNAME) {
        this.TOKEN = TOKEN;
        this.USERNAME = USERNAME;
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            String message_text = update.getMessage().getText();
            long chat_id = update.getMessage().getChatId();
            log.info("Бот получил от " + chat_id + ", сообщение " + message_text);
            if (message_text.startsWith("/start")) {
                SendMessage message = new SendMessage()
                        .setChatId(chat_id)
                        .setText("Бот рассчитывает информацию по заданному ip адресу и маске.\n" +
                                "Пример: /ip 192.168.100.1/25");
                try {
                    execute(message);
                } catch (TelegramApiException ex) {
                    log.error(ex.getMessage());
                }
            } else if (message_text.startsWith("/ip")) {
                String ipAddress = message_text.split(" ")[1];


                IPCalc ip = new IPCalc(ipAddress);
                log.info("ip: " + ipAddress);
                log.info("Is valid: " + ip.verify());
                log.info("IP: " + ip.getIpAddress());

                SendMessage message = new SendMessage()
                        .setChatId(chat_id)
                        .setText("<b>[IP_CALC - Информация по заданному IP]:</b>\n" +
                                "<i>1.</i> IP адрес:\t\t\t" + ip.getIpAddress() + " - " + IPCalc.visualizeAddress(ip.getBinIpAddress()) + "\n" +
                                "<i>2.</i> Адрес сети:\t\t\t" + ip.getDecNetwork() + " - " + IPCalc.visualizeAddress(ip.getBinNetwork()) + "\n" +
                                "<i>3.</i> Broadcast адрес:\t\t\t" + ip.getDecBroadcast() + " - " + IPCalc.visualizeAddress(ip.getBinBroadcast()) + "\n" +
                                "<i>4.</i> Адрес 1-го узла:\t\t\t" + ip.getDecMinHost() + " - " + IPCalc.visualizeAddress(ip.getBinMinHost()) + "\n" +
                                "<i>5.</i> Адрес последнего узла:\t\t\t" + ip.getDecMaxHost() + " - " + IPCalc.visualizeAddress(ip.getBinMaxHost()) + "\n" +
                                "<i>6.</i> Количество хостов:\t\t\t" + ip.getNHost()   + ".\n" +
                                "<i>7.</i> Класс сети:\t\t\t" + ip.getIpAddressClass() + " (" + ip.getTypeInternet() + ")" + ".")
                        .enableHtml(true);
                try {
                    execute(message);
                } catch (TelegramApiException ex) {
                    log.error(ex.getMessage());
                }
            } else {
                SendMessage message = new SendMessage()
                        .setChatId(chat_id)
                        .setText("Unknown command :/ .");
                try {
                    execute(message);
                } catch (TelegramApiException ex) {
                    log.error(ex.getMessage());
                }
            }

        }
    }

    @Override
    public String getBotUsername() {
        return USERNAME;
    }

    @Override
    public String getBotToken() {
        return TOKEN;
    }
}
