package su.sviridoff.net;

import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.framework.props.Parameters;
import su.sviridoff.net.telebot.IPCalcBot;

public class TelegramBot {

    private static Parameters parameters = new Parameters("./IPCalcBot.properties");
    private static Logger logger = new Logger(TelegramBot.class.getName());

    public static void main(String[] args) {

        String token = (String) parameters.getValue("TELEGRAM_TOKEN");
        String username = (String) parameters.getValue("TELEGRAM_USERNAME");

        ApiContextInitializer.init();

        TelegramBotsApi botsApi = new TelegramBotsApi();

        try {
            botsApi.registerBot(new IPCalcBot(token, username));
            logger.info("Telegram bot для расчета информации по заданному ip адресу.");
        } catch (TelegramApiException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }


    }


}
