FROM openjdk:8-jdk-alpine

ENV TELEGRAM_TOKEN
ENV TELEGRAM_USERNAME IPCalcGBBot

ARG JAR_FILE=target/IP_Calc-1.0-SNAPSHOT-jar-with-dependencies.jar
ARG JAR_PROPERTY_FILE=IPCalcBot.parameters

WORKDIR /usr/local/runme

COPY ${JAR_FILE} app.jar
COPY ${JAR_PROPERTY_FILE} .

ENTRYPOINT ["java","-jar","app.jar"]
